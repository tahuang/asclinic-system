#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/UInt16.h"
#include "std_msgs/Float32.h"

#include "i2c_driver/i2c_driver.h"

#include "pololu_smc_g2/pololu_smc_g2.h"

#include "pca9685/pca9685.h"

#include <bitset>

// Include the asclinic message types
#include "asclinic_pkg/ServoPulseWidth.h"
#include "asclinic_pkg/Motor.h"

// Namespacing the package
using namespace asclinic_pkg;

double shared_dis;

void templateSubscriberCallback(const std_msgs::UInt16& msg){
    double dis = msg.data;
    shared_dis = dis;
    //ROS_INFO("distance is %f", dis);
    //ROS_INFO("[CUSTOMNODE] Message receieved with data = %f", dis);
}



int main(int argc, char **argv){

    ros::init(argc, argv, "controlmotor");
    ros::NodeHandle nodeHandle("~");
    ros::NodeHandle node_handle_for_global("/my_global_ns");
    ros::Subscriber tof_distance_subscriber = node_handle_for_global.subscribe("tof_distance", 1, templateSubscriberCallback);
    //ros::spin();
   // templateSubscriberCallback.msg.data

    ros::Publisher topic_publisher = node_handle_for_global.advertise<asclinic_pkg::Motor>("set_motor_duty_cycle", 10, false);
    ros::Rate loop_rate(10);


while (ros::ok()){
    //ros::spinOnce();
    ROS_INFO(" data = %f", shared_dis);
    asclinic_pkg::Motor vol;
    if(shared_dis <300){
        vol.function = 1;
        vol.velocity = 0;
    }
    else { 
        vol.function = 1;
        vol.velocity = 10;
    }

    topic_publisher.publish(vol);
    
    ros::spinOnce();
    loop_rate.sleep();

}



    return 0;
}